# Safety in protest

Compiling tips for safety in protest

## Additional resources

* [Protestos.org](https://protestos.org/) - with guide in العربية, English, Français, Português, Español,
* [Attending a protest](https://ssd.eff.org/en/module/attending-protest) by Surveillance Self Defense (EFF)
* [Safety during protest](https://www.amnestyusa.org/pdfs/SafeyDuringProtest_F.pdf) by Amnesty International
* [Surveillance Self Defense: Attending Protest in the Age of COVID-19](https://www.eff.org/deeplinks/2020/06/surveillance-self-defense-attending-protests-age-covid-19) 
* [A protest guide in solidarity from Beirut](https://github.com/frombeirutwithlove/ProtestTips)
* [Digital Security Tips for Protesters](https://www.eff.org/deeplinks/2016/11/digital-security-tips-for-protesters)
* [Digital Security Advice for Journalists Covering the Protests Against Police Violence](https://www.eff.org/deeplinks/2020/06/digital-security-advice-journalists-covering-protests-against-police-killings)
* [Digital security policy for protests](https://www.phillysocialists.org/digital-security)
* [Going to a Protest? Here's How to Protect Your Digital Privacy](https://time.com/5852009/protest-digital-privacy/)
* [How to secure your phone before attending a protest](https://www.theverge.com/21276979/phone-protest-demonstration-activism-digital-how-to-security-privacy)
* [How to Protest Safely in the Age of Surveillance](https://www.wired.com/story/how-to-protest-safely-surveillance-digital-privacy/)
* [Cyber Security for Protests](https://isc.sans.edu/forums/diary/Cyber+Security+for+Protests/26210/)

### Documenting during protest 

Arul Prakkash, WITNESS Senior Manager of Programs for Asia and Pacific as well as a Coconet community member, has written and compiled the following resources for those who will be participating in protests.

- Infographic: [Filming protests and demonstrations](http://bit.ly/2pZEzjS)
- Infographic: [Covering protests in teams](https://wit.to/3hI2Ntm)
- Filming in teams: [Protests, demonstrations, and rallies](https://wit.to/2JtrCc9)
- [Filming protests, demonstrations, and police conduct](https://wit.to/2Di4yMx)
- [Documenting during Internet shutdowns](https://blog.witness.org/2020/02/documenting-during-internet-shutdowns/)




